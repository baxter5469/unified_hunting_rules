---
layout: default
title: Unified Achievement Hunting Rules
permalink: /
---

# Unified Achievement Hunting Rules

_Last changed on: {{ page.last_modified_at | date: "%Y-%m-%d %H:%M" }}_

##### Detailed Rules & Regulations Guidebook for Achievement Hunting

## Mission Statement & Philosophy

#### Users are required to earn their achievements fairly and legitimately.

#### **In general, a player must unlock achievements:**

#### **inside of a game by in-game actions;**

#### **by their own control;**

#### **without outside-modifying game files or game code;**

#### **without using another player’s save file, and;**

#### **without using outside cheat programs.**

#### These rules are designed to keep players honest in their gameplay and honest towards each other in competition. It is important to understand that “achievement hunting rules” are not the rules for how to have fun playing a game or how to play a game as it was intended. Achievement Hunting is a community very much like speedrunning or high-score attack where we are playing our games in a non-standard, meta-competitive way. Some rules may seem contradictory because there are certain methods that are not considered “cheating” here even though they are unorthodox in normal play - those rules will be explained below, but it is because they still meet the “physical, mechanical, or digital” conditions needed to trigger those achievements in-game. These rules have been voted on, heavily discussed, and adjusted over the years by experienced achievement hunters since achievements were first released on Steam in 2007 🏆.

#### These Rules Officially Followed By:

#### ![Steam Hunters][icon-sh] [Steam Hunters][sh], ![MetaGamerScore][icon-mgs] [MetaGamerScore][mgs], ![Exophase][icon-exo] [Exophase][exo], ![RetroAchievements][icon-ra] [RetroAchievements][ra], ![100Pals][icon-pals] [100Pals][pals]

----

## Table of Contents

+ ❌ [Not Allowed - General Rules](#-not-allowed---general-rules-) ❌
+ ✅ [Allowed - General Rules](#-allowed---general-rules-) ✅
+ 🎮 [Game-By-Game Rules / Edge Cases](#-game-by-game-rules--edge-cases-) 🎮
+ 😓 [Penalties for Cheating](#-penalties-for-cheating-) 😓
+ 👮 [How to Clean & Appeal Cheating](#-how-to-clean--appeal-cheating-) 👮
+ 📋 [Feedback](#-feedback-) 📋
+ ✏️ [Changelog](#️-changelog-️) ✏️

## ❌ Not Allowed - General Rules Index ❌

+ [NA-1](#na-1): Do not EVER use programs that circumvent game code to instantly unlock/relock achievements, such as Steam Achievement Manager (SAM).
+ [NA-2](#na-2): Do not use save files that are not your own.
+ [NA-3](#na-3): Do not modify game code.
+ [NA-4](#na-4): Do not edit registry values for games.
+ [NA-5](#na-5): Do not use outside game programs known as “trainers” or the Cheat Engine program.
+ [NA-6](#na-6): Do not use unofficial mods that alter gameplay.
+ [NA-7](#na-7): Do not use “Achievement Unlocker” mods.
+ [NA-8](#na-8): Do not use TAS (tool-assisted speedrunning), Bots, or Complex Scripting to gain achievements.
+ [NA-9](#na-9): Do not alter the system clock in order to unlock achievements that require a passage of time.
+ [NA-10](#na-10): Do not use console commands or debug menu options which allow you to speed up the passage of time.
+ [NA-11](#na-11): Do not use any “depot method” to access “easier” versions of games.
+ [NA-12](#na-12): Do not use console commands which directly and instantly unlock achievements.
+ [NA-13](#na-13): Do not allow other users to access your account and earn achievements for you.
+ [NA-14](#na-14): Do not be abusive to developers.
+ [NA-15](#na-15): Do not create or share cheating methods.

## ✅ Allowed - General Rules Index ✅

+ [OK-1](#ok-1): Console commands that do not directly/instantly unlock achievements are allowed.
+ [OK-2](#ok-2): In-game cheat codes/modes and in-game debug tools are allowed.
+ [OK-3](#ok-3): Making use of in-game glitches, in-game exploits, and “cheesy” tactics are allowed.
+ [OK-4](#ok-4): “Save-scumming” and copying/transferring your own save files are allowed.
+ [OK-5](#ok-5): Using online guides/walkthroughs is allowed.
+ [OK-6](#ok-6): “Clickers / Autoclickers / Macros” are allowed.
+ [OK-7](#ok-7): Getting “carried” or “twinked” in coop games is allowed.
+ [OK-8](#ok-8): “Farming” and “boosting” achievements is allowed.
+ [OK-9](#ok-9): Changing your OS system clock to get Date-Specific achievements is allowed.
+ [OK-10](#ok-10): Deleting Registry Values is allowed.
+ [OK-11](#ok-11): Use of in-game buttons and button combinations that developers add into the game to unlock achievements is allowed.
+ [OK-12](#ok-12): Using in-game/in-app DLC and microtransactions (“p2w” / “Pay-to-Win”) is allowed.
+ [OK-13](#ok-13): Creating or using custom maps for games that support custom maps is allowed.
+ [OK-14](#ok-14): Simplistic Scripting is allowed to facilitate gameplay but not fully replace/emulate gameplay.
+ [OK-15](#ok-15): The “Depot Method” is allowed for games that broke themselves with an update.
+ [OK-16](#ok-16): Most Steam Workshop Mods and Official Mods/Official Patches are allowed.
+ [OK-17](#ok-17): Fully Cosmetic Mods are allowed.
+ [OK-18](#ok-18): Modifying certain standard settings via .ini files is allowed.
+ [OK-19](#ok-19): Reading game files and game code is allowed.

## ❌ Not Allowed - General Rules ❌

### NA-1

### Do not EVER use programs that circumvent game code to instantly unlock/relock achievements, such as Steam Achievement Manager (SAM).

Even if you “should have” unlocked the achievement. Even if the game is bugged. Even if the achievement is removed or broken entirely. This is extremely easy to detect because of the way it interacts with game stats in the API pull and is harshly punished. There is NO situation where using SAM-type programs to unlock/relock an achievement is allowed, except for appeals with moderator direction.

>Examples:
>Using SAM to unlock the broken achievement in Hate Plus;
>using SAM to unlock DLC achievement in a game you previously 100%ed before DLC was added;
>using SAM to modify a game stat to be 499/500 and then getting the last one yourself;
>using an achievement locker/unlocker to relock achievements you do not want; etc.

### NA-2

### Do not use save files that are not your own.

Even if your save got lost/corrupted or you need a save from earlier in the game because you missed something.

>Examples:
>Downloading a clicker game save online;
>begging a friend to send you his save file because your house burned down and you lost all your PC files;
>etc.

### NA-3

### Do not modify game code.

Do not use other people's unofficial edits of game code, even if it seems to "fix" a game. If an achievement is broken, please notify the developers of the game and wait for an update patch. Simply assuming developer intent and altering game code to unlock achievements misleads developers with false statistics, and the trade of potentially malicious compiled objects such as .dll and .exe files is a large security risk. Please see [Allowed Rule 18](#ok-18) and any relevant [Game-by-Game Edge Cases](#-game-by-game-rules--edge-cases-) for the only exceptions to this rule.

>Examples:
>Opening up specific game files with a text/hex editor and changing values to what you want;
>editing properties such as “read-only” for game files;
>editing TF2 files and pointing it towards a YouTube video you do not own for the YouTube views achievements;
>swapping certain .dll files or other game files between similar games to attempt to “fix” broken games;
>editing the images of puzzle pieces in puzzle games to have a number so you see exactly where to put them;
>modifying an .ini file for a Unity game to change enemy health;
>etc.

### NA-4

### Do not edit registry values for games.

>Examples:
>Going into the registry folder for a game and updating a registry file that keeps track of how many kills you have to get closer to a “Kill 1000 enemies” type of achievement;
>etc.

### NA-5

### Do not use outside game programs known as “trainers” or the Cheat Engine program which can boost your powers in games, unlock debug modes, speed the game up, modify game code, etc.

>Examples:
>Using Cheat Engine for anything;
>downloading a Devil May Cry trainer to enable invincibility/infinite Devil Trigger;
>downloading and using cheat toolsets or trainers for GTA V;
>using unofficial mods that act as trainers or debug/cheat toolsets;
>Game Genie / Action Replay and other such similar cheat tools;
>etc.

### NA-6

### Do not use unofficial mods that alter gameplay.

All Steam Workshop mods are allowed except for mods that instantly/directly unlock achievements.

Outside of Steam Workshop, only fully cosmetic mods are allowed. No unofficial mods that alter gameplay are allowed.
Please see [Allowed Rule 16](#ok-16) and [Allowed Rule 17](#ok-17) below for more information on allowed mods.

>Examples:
>Installing unofficial debug-style mods similar to a trainer;
>Installing unofficial and gameplay-altering mods from sites such Nexus Mods or GameBanana or ModDB or random forums;
>installing a mod from NexusMods that increases your carry capacity or increases your XP gain or enables fast travel and other such gameplay-altering mods;
>etc.

### NA-7

### Do not use “Achievement Unlocker” mods for games.

Do not use “Achievement Unlocker” type mods that grant options such as a teleport power you can use to go to an achievement room with buttons you simply press for achievements or items you can use that unlock achievements directly/instantly, etc.

>Examples:
>Achievements Unlocked mod for skyrim that unlocks all achievements as soon as you start the game;
>do not use any mods that allow you to instantly/directly unlock achievements via command/button/power;
>etc.

### NA-8

### Do not use TAS (tool-assisted speedrunning), Bots, or Complex Scripting to gain achievements.

These are all advanced scripting of a game far beyond simple tasks (sometimes even synced to clockspeed) to allow complex, unique, and/or non-monotonous/non-repetitive sections of a game to be played without human interaction. (See [Allowed Rule 14](#ok-14) below for allowed simple scripts).

>Examples:
>Using a bot for an MMO game to gather resources/level up for you;
>using a TAS to complete a difficult speedrun or no-death achievement;
>using a complex script with advanced functionality (such as PixelSearch/ImageSearch, OCR, AI/ML, etc.) and/or decision-making ability;
>using complex scripts that complete entire games or large portions of games with no human input but requiring many unique actions such as completing Portal 1’s gold challenge rooms;
>etc.

### NA-9

### Do not alter the system clock in order to unlock achievements that require a passage of time.

See [Allowed Rule 9](#ok-9) below for allowed date-change rules.

>Examples:
>The Stanley Parable’s achievement to not play for five years;
>Garry’s Mod/Universe Sandbox achievements to play for one year;
>Tabletop Simulator’s 1000 hours achievement;
>Chivalry: Medieval Warfare’s 80 hours achievement;
>etc.

### NA-10

### Do not use console commands or debug menu options which allow you to speed up the passage of time in order to more quickly achieve passage of time-based achievements such as “play for x hours”.

>Examples:
>aoc_slomo in Chivalry: Medieval Warfare;
>“survive 100 days” achievement in The Forest;
>“1001 Nights” achievement in Cities: Skylines;
>etc.

### NA-11

### Do not use any “depot method” to access “easier” versions of games.

Depot methods are only to be used as described in [Allowed Rule 15](#ok-15) below.

>Examples:
>If a developer updates their game and changes the “difficulty” or design of a game then you are not allowed to depot back to an easier time in the game’s design history;
>etc.

### NA-12

### Do not use console commands which directly and instantly unlock achievements.

>Examples:
>“caqs” in Skyrim and Fallout 4;
>“unlock ach” in Into The Breach;
>anything similar to a “unlock_achievement x” type of command;
>etc.

### NA-13

### Do not allow other users to access your account and earn achievements for you (including your “little brother” or your “cousin”).

You must earn your achievements yourself. Letting people earn achievements on your account is not allowed in general and if anyone cheats or breaks the rules on your account, you will still be held accountable.

>Examples:
>Having a friend who is more skilled at a game complete a difficult section for you in a single-player game;
>letting someone log into your Steam account to beat a difficult part of a game for you on your account;
>letting your little brother work on a grindy achievement for you;
>etc.

### NA-14

### Do not be abusive to developers via their Steam forums, social media, email, doxxing, etc.

You’re allowed to complain, ask for changes, leave negative reviews/comments and other such respectful criticisms, but you give all of us in the achievement hunting community a really bad name when you harass/attack developers because of what they did to their game’s achievements. Do not abuse Steam’s refund policy by purchasing a game, quickly 100%ing (hundoing) it, and then refunding it (“refundo”).

>Examples:
>Issuing death threats or flagrant insults to devs because they added achievements to their game that you do not like;
>using extreme/excessive rudeness while demanding a developer remove or change achievements;
>etc.

### NA-15

### Do not create or share cheating methods.

>Examples:
>Teaching someone how to use SAM;
>sharing a non-allowed complex script;
>posting on the Steam forums to promote an egregious game file edit method;
>sending someone a link to Cheat Engine or a trainer;
>teaching someone how to hide cheating;
>creating a non-allowed complex script and sharing it around even if you never use it yourself;
>etc.

## ✅ Allowed - General Rules ✅

### OK-1

### Console commands that do not directly/instantly unlock achievements are allowed.

>Examples:
>“sv_cheats 1, x, sv_cheats 0”;
>god;
>tgm;
>spawn enemy;
>npc_kill;
>noclip;
>godmode;
>fly;
>most Steam console commands;
>etc.

### OK-2

### In-game cheat codes/modes and in-game debug tools are allowed.

>Examples:
>iddqd
>cheat code style commands typed into older FPS games or cheat options unlocked in certain games such as Duke Nukem 3D;
>speedup options in Steam’s Final Fantasy re-releases;
>etc.

### OK-3

### Making use of in-game glitches, in-game exploits, and “cheesy” tactics are allowed.

>Examples:
>breaking out out of normal bounds to avoid enemies;
>doing something strange inside of a game to “bug a game out” so you become invincible;
>accessing powerful items earlier than intended;
>standing somewhere enemies can’t hit you;
>etc.

### OK-4

### “Save-scumming” and copying/transferring your own save files are allowed.

Please realize that transferring save files between platforms may cause identical unlock timestamps for the achievements that were imported. Having proof of your original progress is strongly recommended.

>Examples:
>quicksaving/quickloading often;
>manually backing up a copy of your own save file and moving it back into your save folder after a game had deleted it;
>manually backing up a copy of your own save file and moving it back into your save folder in order to replay sections of a game before an important story junction;
>using your own Steam Cloud on multiple PCs to play the same game;
>transfering your own save file to a new PC;
>importing your mobile save file for Time Clickers, Final Fantasy Mobius, Realm Grinder, etc. (See: <https://goo.gl/imGkqk> for a list of mobile games with Steam transfer capabilities);
>transferring GTA V save between console and Steam;
>etc.

### OK-5

### Using online guides/walkthroughs is allowed.

>Examples:
>Steam guides;
>youtube video guides;
>GameFAQs;
>puzzle solutions;
>collectible lists;
>etc.

### OK-6

### “Clickers / Autoclickers / Macros” are allowed.

A clicker is defined as hardware or software that manually or digitally mimics button presses. Clickers/autoclickers typically press one key rapidly or at a specific pace whereas macros might press 3 or more different keys in a specific order again and again. These tools are only allowed if they are for monotonous simple tasks and if they meet the “mechanical” conditions of achievement. This could be a weight on a key, a rubber band on a thumbstick, a turbo controller, an autoclicker program (AutoClicker, NIAutoClicker, FastClicker, etc.), a macro program (Joy2Key, Macro Recorder, etc.), and many mouse drivers come with macro programs installed by default as well.

>Examples:
>Turning on an autoclicker in a clicker game to rapidly click the enemy;
>making a simple macro that forces your character to jump over and over to earn a “jump x times” achievement;
>using an autoclicker to rapidly fire your gun in a space shooter style game;
>letting an auto-clicker rapidly left-click through an entire kinetic novel;
>etc.

### OK-7

### Getting “carried” or “twinked” in multiplayer games is allowed.

>Examples:
>a skilled friend playing some Payday heists or Killing Floor games with you at a higher level than you could handle alone;
>a friend inviting you into their Terraria world and helping you kill some bosses;
>high-level friends donating gear to you;
>etc.

### OK-8

### “Farming” and “boosting” achievements is allowed.

Note that although this is allowed in general for achievement hunting, many various online game communities are very against this sort of organized farming and will ban you for it.

>Examples:
>trading kills back and forth in a shooter game;
>camping some location for a special item again and again;
>having one friend join and leave your game so you auto-win over and over;
>running a program such as Sandboxie in order to have multiple instances of a game open and farm yourself;
>etc.

### OK-9

### Changing your OS system clock to get Date-Specific achievements is allowed.

Date-changing is allowed only for date-specific achievements that specify a certain single date. Date changing is not allowed to produce a passage of a specific amount of time (See [Not Allowed Rule 9](#na-9)). Changing your OS system clock to complete more “daily challenge” type achievements without waiting is allowed also. Date-changing for date-specific achievements is allowed because of many games having many issues relating to time zones or natural de-sync with PCs, but these issues should not be present for longer-term “passage of time” achievements.

>Examples:
>changing your PC’s date to the twelve dates needed for Calendar Man’s “Storyteller” achievement in Batman Arkham City;
>changing date to April 29th for “Happy Birthday Pixel!” in Cave Story+;
>completing multiple LYNE daily puzzles within a single day by changing OS date;
>etc.

### OK-10

### Deleting Registry Values is allowed.

Sometimes games will break or add achievements and require a full reset to unlock further achievements - this is common with idle/spam games where achievements are triggered through registry values. You are allowed to delete those registry entries and reinstall the game to re-idle the time and try again to get it to trigger properly. You are not allowed to “edit” registry values (See Not Allowed Rule 4) to any specific number or value.

>Examples:
>Many various spam/idle type games require either a registry wipe or playing on a new PC to “reset” the game and thereby re-idle the needed time (often for newly-added achievements or patches to these types of games);
>etc.

### OK-11

### Use of in-game buttons and button combinations that developers add into the game to unlock achievements is allowed.

Note that using one of these may get your game auto-invalidated on AStats since the rules/enforcement on this have been extremely varied on AStats over the years. We will not penalize users for these actions here because this type of action is done fully in-game and we cannot really control what devs add to their games.

>Examples:
>Incredipede;
>Redactem;
>Sweatshop;
>Daily Run;
>Plandzz;
>Rogue Legacy;
>etc.

### OK-12

### Using in-game/in-app DLC and microtransactions (“p2w” / “Pay-to-Win”) is allowed.

>Examples:
>Purchasing gems in Crush Crush with real world money;
>purchasing items/in-game currency with real world money to get a boost in many clicker games and f2p (Free-to-Play) games;
>purchasing powerful items with real world money in Warframe or TF2;
>etc.

### OK-13

### Creating or using custom maps for games that support custom maps is allowed.

They still need to follow the other rules in terms of not auto-unlocking achievements, etc.

>Examples:
>Creating or using custom maps in: Counter-Strike Source, CS:GO, Day of Defeat: Source, Pirates vs. Vikings II, Booster Trooper, ORION: Prelude;
>etc.

### OK-14

### Simplistic Scripting is allowed to facilitate gameplay but not fully replace/emulate gameplay.

“Simple Scripting” is a macro or sequence of commands that helps facilitate “monotonous/repetitive” gameplay but does not entirely replace gameplay. Complex Scripting such as TAS-type scripts or Bots (typically used for MMOs) are not allowed (See Not Allowed Rule 8 above). Many of these simple scripts are written in AutoHotKey (AHK).

>Examples:
> Writing a script to have your second PC and alt steam account auto-join your lobby and ready up so you can farm kills/wins against it;
>creating a .bat file to open and close a game rapidly for “open game x number of times” achievements as found in Garry’s Mod;
>simple AHK scripts to click multiple places on the screen at different intervals in clicker games;
>simple AHK scripts to start/end a game for “play as x character y times” type achievements such as in Metal Slug;
>etc.

### OK-15

### The “Depot Method” is allowed only for games that broke themselves with an update.

This is a valid method of obtaining older or unique versions of games directly from Steam. This method is allowed because we can accurately determine where these files were sourced from (Valve) and obtaining them through Steam directly instead of a third party warez/torrent site does not break any copyright law, TOS, GNU/GPL, or SSA. This method is only allowed to obtain a working version of a game if the current present date version of the game is broken, or an achievement is broken due to a patch/update. This is not allowed to be used to access “easier” versions of a game (See [Not Allowed Rule 11](#na-11)).

>Examples:
>Ubinota - A March 28th, 2015 patch to the game seemingly broke the functionality for the achievement “Challenge n°1”, preventing the trigger from working properly. You can depot back to the March 4th, 2015 version of the game where the achievement functionality works;
>etc.

### OK-16

### Most Steam Workshop Mods and Official Mods/Official Patches are allowed.

Steam Workshop mods are considered official and generally allowed (even Steam Workshop mods that alter gameplay) except in the case of “Achievement Unlocker” mods (See [Not Allowed Rule 7](#na-7)).

>Examples:
>Installing and using most Steam Workshop mods, as long as they do not grant you ways to instantly/directly unlock achievements ([Not Allowed Rule 7](#na-7));
>installing official, dev-provided H-patches (Hentai / Adult Content patches) which were downloaded outside of Steam in games where this is required for an achievement (many VN hentai games);
>downloading and using dev-provided accessory tools/patches to games;
>etc.

### OK-17

### Fully Cosmetic Mods are allowed.

100% cosmetic-only mods are allowed, as long as they completely do not alter gameplay - this includes: texture packs, accessibility mods, mesh/lighting mods, nude mods, character portrait mods, mods that change how your player character looks, widescreen/aspect ratio mods, FoV mods, UI mods, HUD mods, and graphics overhaul mods. Unofficial mods that enable see-through walls/doors, wallhacking, outline/glow of enemies/items/etc. are not considered “cosmetic” and not allowed. Note that using any mods at all for online games might get you banned or VACed.

>Examples:
>Installing high-resolution texture pack mods or lighting fixes for The Witcher 3 is allowed;
>changing the character portrait artwork for the villagers in Stardew Valley is allowed;
>reskinning your Skyrim character into a beautiful naked magical hentai pony is allowed;
>etc.

### OK-18

### Modifying certain standard settings via .ini files is allowed.

Certain games contain an Initialization file (.ini file) which may be the only way to configure standard in-game options such as: resolution, aspect ratio, v-sync, windowed/fullscreen mode, directx version, standard keybindings, etc. Some games (namely Source games) use .ini files which can mimic “Launch Options” under a Steam game’s properties view or can auto-launch standard console commands upon running the game - these are allowed. Modifying any .ini file value outside of these typical (in-game visual/audio settings / console commands / launch options / standard keybinds) type of situations is not allowed and will be considered as game file editing, unless a specific game-by-game rule exists below.

>Examples:
>Changing an .ini file to the exact 4:3 resolution your older monitor needs because the in-game settings only allow for widescreen;
>enabling v-sync for a game via .ini file because it lacks an in-game setting;
>hard-forcing directx9 for a game via .ini file;
>etc.

### OK-19

### Reading game files and game code is allowed.

As long as you are not modifying any actual game file or game code, you are allowed to open game files/registry entries and simply read them. Unzipping/decompiling/decrypting a game file in order to read it is allowed as long as it does not violate either the game’s or Steam’s Terms of Service.

>Examples:
>Opening up a ren.py Visual Novel game’s text file to see when a certain achievement unlocks;
>etc.

## 🎮 Game-By-Game Rules / Edge Cases 🎮

### Air Forte

✅ AStats allowed for the renaming of one file. Without the renaming of that file, all achievements do not trigger. The game calls the triggers to a steamwrapper.dll that is misnamed. Going to `C:\\Program Files\\Steam\\SteamApps\\common\\airforte\\data` and renaming the .dll from steamwrapper.dll to steamwrap2.dll will allow the game to properly trigger achievements. The file in question is still the file the game comes with and not a file from any other game. We will not penalize users for doing this because this is a long-established, unique AStats ruling.

### aMaze Achievements : Darkness

✅ AStats allowed the copy and pasting of an identical "sister" file “Assembly-CSharp.dll” from aMaze Achievements: Forest to "fix" aMaze Achievements: darkness by replacing the “Assembly-CSharp.dll” file in Darkness without penalty. AStats allowed this due to previous rulings on other games that involved copying files you own from one game to another version of the same game, specifically Red Orchestra 2 Single Player into Multiplayer. We will not penalize users for doing this because this is a long-established, unique AStats ruling.

### Company of Heroes 2

❌ Use of the Steam Workshop Mod “Achievement M.10 - Zero-Risk Market Unlocker” is not allowed as it exhibits SAM-like behavior in auto-unlocking an achievement upon loading a map.

### Dungeons of Dredmor

❌ The workshop mod for “You Used All The Glue on Purpose” (the wand achievement) is not allowed as it uses a custom skill tree and does not meet the criteria of the achievement (repair 500 wands with the n-Dimensional Lathe).

### Eversion

✅ What Have You Done Achievement. It requires you to edit a mock, fake .txt file with any text and save it to then trigger the achievement the next time you start the game. We allow this as it is required for the Achievement and does not interact with any other configurations for the game. The fake empty .txt file was put there specifically by the developer to use for this achievement.

### Garry’s Mod

✅ On Christmas Eve, 2015 a large amount of achievement hunters were tricked by a “fake Garry” in a public server who was actually a hacker troll granting people the “Yes, I am the real Garry!” achievement. Typically this would be against the rules for achieving something via hacking instead of fulfilling the real requirements, but since so many achievement hunters were affected that day, it was deemed to be allowed for this one case.

### Killing Floor

✅ Clarification: Dedicated servers do not disable achievements and perk progression while using any mutators, including Steam Workshop mods. This is permitted under [Allowed Rule 3](#ok-3) and [Allowed Rule 16](#ok-16).

More Info: Killing Floor 1 has a somewhat unique system of whitelisting mutators/mods (See: [KF1 Mutator Whitelist](https://kf-wiki.com/wiki/List_of_whitelisted_and_greylisted_mutators)). In solo mode and listen server mode the whitelist is properly applied and non-whitelisted mutators will disable achievement and perk progress. However, dedicated servers do not disable achievement and perk progress when using non-whitelisted mutators/mods, possibly due to a bug.

### Microtransaction Simulator

✅ The “Brick your game (intentionally)” achievement requires you to open a game save file and type some gibberish and then open your game, which will result in crashing the game and earning the achievement - this is normally disallowed game file editing ([Not Allowed Rule 3](#na-3)) but is allowed in this case. ([Steam Guide](https://steamcommunity.com/sharedfiles/filedetails/?id=1146799068))

### Payday 2

✅ Use of Visual HUD mods/UI Improvement mods are allowed (HoxHUD/WolfHUD). While Payday 2 does not use Steam Workshop, a good amount of these mods have been “approved” by the developers.

❌ This does not allow other lua scripting/mods that alter the game in other ways outside of HUD/UI and do things such as: auto grant XP, increase XP gain, despawn enemies, grant achievements directly, allow more players in a game, allow players to carry more bags, increase loot, grant infinite ammo, grant invulnerability, etc.

❌ Playing with hackers or people obviously using these types of game-altering mods mentioned in point 2 above is not allowed. Please leave the game as soon as possible after noticing a hacker and realize that playing with a hacker may render your entire game invalidated as it will make your achievement and/or in-game stats appear as cheated.

### Pixel Puzzles Ultimate

✅ Deleting a local copy of a file used to store Christmas event “advent calendar” data allowed users to gain near-infinite amounts of in-game currencies such as hints and golden puzzle pieces. While file deletion was “okayed” by AStats mods, users went further by changing file properties (specifically, making the file read-only), to speed up the process of deleting the save file again and again. This breaks the standard “[do not edit game files](#na-3)” rule but is allowed in this one case.

### Polynomial

✅ This is just a clarification that what is describe here is allowable. The game uses a GUI to change level settings and game settings. Within that function you can also import scripts. In imported scripts you can do much more and have many more options that can be changed than what the GUI or game documentation states. An in-game editor simply serves as an interface for a user to edit the script in a more comfortable fashion, and it being feature-incomplete does not necessarily represent those features being intentionally unsupported/denied. This is written just to clarify that using the import function built into the game for scripts and doing more than what the GUI says is available is allowable as long as those scripts do not auto unlock/trigger achievements directly like SAM or SAM-like mods would function.

### Red Orchestra 2

✅ The Developers, Tripwire removed the Singleplayer mode from the Base Game of Red Orchestra 2, now renamed Red Orchestra 2 Multiplayer, and moved it to Red Orchestra 2 Singleplayer. The achievements still exist in the base game achievements list. AStats allowed users to copy the SinglePlayer folder entirely over to the root folder of the Multiplayer Game to allow for play and earning of Singleplayer Achievements. We also allow it and will not penalize users for this.

### Saints Row: The Third / Saints Row IV

✅ [My Steelport Emulator](https://steamcommunity.com/sharedfiles/filedetails/?id=1803368990) is allowed for the Steam Version of the games for obtaining the achievements 'Jumped In' & 'Saintified'. Unlike most 'achievement fixing' mods, this one DOES NOT modify any game files.

❌ My Steelport Emulator for the GOG version of the games is not allowed as the method for it includes file editing.

### Skyrim

✅ Downloading the non-Steam Workshop “Skyrim Script Extender” for use with various official mods, including SkyUI, is allowed.

### Ted By Dawn

✅ Ted by Dawn’s exe is actually a .zip compressed file not a traditional exe. Within that .zip folder is a steamapi.dll. This dll is outdated and does not properly trigger achievements. AStats allowed users to open that exe/zip and replace the dll with a working dll from any steam game available, usually the steamapi.dll from “Super Blue Boy Planet” a free to play game and dll downloaded directly from steam. Create new text file with name "steam_appid.txt" and insert value to file "381090", then save.

### Trials 2

❌ There is an unlisted API that is used by RedLynx to grant the "True Fan" achievement. It is NOT allowed for non-RedLynx employees to use as per [Not Allowed Rule 1](#na-1).

### Zero Gear

✅ BFF Achievement. AStats allowed you to use a console command to unlock this achievement. We do not allow use of console commands to directly unlock achievements, but we will not penalize users for this specific achievement as this AStats rule has been established too long for us to effectively disallow it. We do discourage users from doing it. All other console commands that directly unlock achievements in a “get.achievement()” fashion are disallowed (See [Not Allowed Rule 12](#na-12)).

## 😓 Penalties for Cheating 😓

AchievementHunting.com is in partnership with and/or shares data with most of the achievement trackers, including [completionist.me](https://completionist.me/), [Exophase](https://www.exophase.com/), [MetaGamerScore](https://metagamerscore.com/), [Steam Hunters](https://steamhunters.com/), [TrueSteamAchievements](https://truesteamachievements.com/), and [RetroAchievements](http://retroachievements.org/).

Breaking [Not Allowed Rule 1](#na-1), [Not Allowed Rule 3](#na-3), or [Not Allowed Rule 5](#na-5) are each considered critical offenses and can result in you being immediately fully banned from achievement tracker leaderboards. Additionally, the user will be publicly marked as a cheater and also lose “verified” status on the 100Pals Discord until they complete cleaning and appeal via the [AchievementHunting.com Appeal Process](https://forum.achievementhunting.com/viewtopic.php?f=3). 
Other forms of cheating and “Not Allowed” actions as described above can result in the point value of those individual games being invalidated and marked as cheated on a user’s tracker profiles. If a user has a substantial amount of invalidated games or purposefully hides their cheating, that also may result in a de-verification on the 100Pals Discord and achievement tracker leaderboards ban until a cleaning and appeal is completed. Violations of [Not Allowed Rule 14](#na-14) or [Not Allowed Rule 15](#na-15) regarding harassing developers and sharing cheat methods can result in losing verification and/or being banned from the 100Pals Community.

If a user refuses to appeal, abandons their appeal, or is dishonest during their appeal, they will be banned from the 100Pals Community and banned on the achievement tracker leaderboards.

## 👮 How to Clean & Appeal Cheating 👮

Various automatic-detection systems and inspector suite toolsets exist for moderation/inspector staff to police cheating across the achievement trackers. Players are also manually deep-dived at random and during initial verification by 100Pals Staff.

Upon ban-worthy cheating being detected, a 100Pals staff member will be in contact with you to let you know that cheating was detected. You will be instructed by the staff member and required to wipe the achievements of those cheated games either by in-game console commands or a relocker tool that the staff member will provide you with (please do not ever use SAM or relocker tools to lock achievements before speaking with a mod). A log of this will be created, archived, and provided to the achievement tracker moderation teams. Please see our [Appeal Forum](https://forum.achievementhunting.com) and [Appeal FAQ](https://forum.achievementhunting.com/viewtopic.php?f=3&t=6).

If you wish to come forward and admit cheating, please PM a moderator on the 100Pals Discord or create an appeal on our [Appeal Forum](https://forum.achievement.hunting.com), this will be greatly appreciated and taken into consideration during your appeal.

*Written with love by:*
*❤️ The 100Pals Staff Team ❤️*

*With generous assistance from:*
*Several moderators and developers of: AStats, Completionist.me, Exophase, MetaGamerScore, and Steam Hunters.*

*And with valuable suggestions and input from dozens of tracker users and community members.*

## 📋 Feedback 📋

If you have any comments or suggestions regarding the above rules you can leave anonymous feedback here: <https://goo.gl/UJ7W9g> or you can chat with us on the [Discord](https://discord.gg/100pals). Additionally, proposed rule changes can be made by making a [Gitlab pull request](https://gitlab.com/100pals/unified_hunting_rules/issues).

These rules will be amended from time to time and game-by-game rules will be created to clarify edge cases as needed.

## ✏️ Changelog ✏️

_Last changed on: {{ page.last_modified_at | date: "%Y-%m-%d %H:%M" }}_

[Git changelog can be found here](https://gitlab.com/100pals/unified_hunting_rules/commits/master/index.md)

[Pre-Git changelog can be found here](/old_changelog)

[cme]: https://completionist.me
[exo]: https://www.exophase.com
[mgs]: https://metagamerscore.com
[pals]: https://100pals.co
[ra]: https://docs.retroachievements.org/Global-Leaderboard-and-Achievement-Hunting-Rules/
[sh]: https://steamhunters.com
[tsa]: https://truesteamachievements.com

[icon-cme]: /images/content/cme_16x16.png "Completionist.me"
[icon-mgs]: /images/content/mgs_16x16.png "MetaGamerScore"
[icon-sh]: /images/content/sh_16x16.png "SteamHunters"
[icon-exo]: /images/content/exo_16x16.png "Exophase"
[icon-ra]: /images/content/ra_16x16.png "RetroAchievements"
[icon-pals]: /images/content/icon-cur-pals.gif "100Pals"
